// ==UserScript==
// @name         TodayHumor ReReComment
// @namespace    http://crisp-surplus.tistory.com
// @version      0.1
// @description  오유에서 대대댓글을 가능하게 합니다.
// @downloadURL  https://bitbucket.org/crispsurplus/todayhumor-rerecomment/raw/master/TodayHumor%20ReReComment.user.js
// @updateURL    https://bitbucket.org/crispsurplus/todayhumor-rerecomment/raw/master/TodayHumor%20ReReComment.user.js
// @author       CrispSurplus (바삭한잉여)
// @match        http://*.todayhumor.co.kr/*
// @grant        none
// ==/UserScript==