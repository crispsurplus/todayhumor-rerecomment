// ==UserScript==
// @name         TodayHumor ReReComment
// @namespace    http://crisp-surplus.tistory.com
// @version      0.3
// @description  오유에서 대대댓글을 가능하게 합니다.
// @downloadURL  https://bitbucket.org/crispsurplus/todayhumor-rerecomment/raw/master/TodayHumor%20ReReComment.user.js
// @updateURL    https://bitbucket.org/crispsurplus/todayhumor-rerecomment/raw/master/TodayHumor%20ReReComment.user.js
// @author       CrispSurplus (바삭한잉여)
// @match        http://*.todayhumor.co.kr/*
// @grant        none
// ==/UserScript==

jQuery(window).ready(function () {
    csou_addReReCmt();
    jQuery('body').append('<style>.reply_img{max-width:100%;}</style>');
});

jQuery(document).ajaxComplete(function () {
    csou_addReReCmt();
});

function csou_addReReCmt() {
    jQuery(".rereMemoWrapperDiv .memoDiv").each(function(i,e){
        if(jQuery(e).find('.csou_ReRe').length == 0) {
            jQuery(e).append('<div class="rereIconDiv csou_ReRe"><img src="http://todayhumor.co.kr/board/images/memo_rere_write.gif" onclick="rere('+e.id.substr(7)+')"></div>');
        }
    });
}